import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import rs.fakultet.seminarski.repository.DBKonektor;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        DBKonektor d = DBKonektor.dajObjekatBaze();
        d.proveriDaLiBazaPostoji();
        Parent root = FXMLLoader.load(getClass().getResource("glavni_prozor.fxml"));
        primaryStage.setTitle("Evidencija radnih sati");
        primaryStage.setScene(new Scene(root, 990, 580));
        primaryStage.setResizable(false);
        primaryStage.resizableProperty().setValue(Boolean.FALSE);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
