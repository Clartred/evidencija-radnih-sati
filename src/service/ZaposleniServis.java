package service;

import rs.fakultet.seminarski.model.Zaposleni;
import rs.fakultet.seminarski.repository.DBZaposleni;

import java.util.ArrayList;

public class ZaposleniServis {

    public ArrayList<Zaposleni> dajZaposlene() {

        DBZaposleni dbZaposleni = new DBZaposleni();
        return dbZaposleni.dajSveZaposlene();
    }

    public void izmeniZaposlenog(Zaposleni zaposleni) {
        DBZaposleni dbZaposleni = new DBZaposleni();
        dbZaposleni.izmeniZaposlenog(zaposleni);
    }

    public void izbrisiZaposlenog(int jmbg) {
        DBZaposleni dbZaposleni = new DBZaposleni();
        dbZaposleni.izbrisiZaposlenog(jmbg);
    }

    public void unesiZaposlenog(Zaposleni zaposleni) {
        DBZaposleni dbZaposleni = new DBZaposleni();
        dbZaposleni.unesiNovogZaposlenog(zaposleni);
    }

    //"Plata veca od 300 evra","Plata manja od 300 evra", "Broj radnih sati manji od 40", "Broj radnih sati veci od 40"
    public ArrayList<Zaposleni> filterPlataManjaOd500() {
        ArrayList<Zaposleni> zaposleni = dajZaposlene();
        ArrayList<Zaposleni> novaListaZaposlenih = new ArrayList<>();
        for (Zaposleni aZaposleni : zaposleni) {
            if (aZaposleni.getZarada() < 500) {
                novaListaZaposlenih.add(aZaposleni);
            }
        }
        return novaListaZaposlenih;
    }

    public ArrayList<Zaposleni> filterPlataVecaOd500() {
        ArrayList<Zaposleni> zaposleni = dajZaposlene();
        ArrayList<Zaposleni> novaListaZaposlenih = new ArrayList<>();
        for (Zaposleni aZaposleni : zaposleni) {
            if (aZaposleni.getZarada() > 500) {
                novaListaZaposlenih.add(aZaposleni);
            }
        }
        return novaListaZaposlenih;
    }

    public ArrayList<Zaposleni> filterBrRadnihSatiManjihOd200() {
        ArrayList<Zaposleni> zaposleni = dajZaposlene();
        ArrayList<Zaposleni> novaListaZaposlenih = new ArrayList<>();
        for (Zaposleni aZaposleni : zaposleni) {
            if (aZaposleni.getBrojRadnihSati() < 200) {
                novaListaZaposlenih.add(aZaposleni);
            }
        }
        return novaListaZaposlenih;
    }

    public ArrayList<Zaposleni> filterBrRadnihSatiVecaOd200() {
        ArrayList<Zaposleni> zaposleni = dajZaposlene();
        ArrayList<Zaposleni> novaListaZaposlenih = new ArrayList<>();
        for (Zaposleni aZaposleni : zaposleni) {
            if (aZaposleni.getBrojRadnihSati() > 200) {
                novaListaZaposlenih.add(aZaposleni);
            }
        }
        return novaListaZaposlenih;
    }
    public ArrayList<Zaposleni> pretraziPoImenu(String ime){
        ArrayList<Zaposleni> zaposleni = dajZaposlene();
        ArrayList<Zaposleni> novaListaZaposlenih = new ArrayList<>();
        for (Zaposleni aZaposleni : zaposleni) {
            if (aZaposleni.getIme().contains(ime)) {
                novaListaZaposlenih.add(aZaposleni);
            }
        }
        return novaListaZaposlenih;
    }
}