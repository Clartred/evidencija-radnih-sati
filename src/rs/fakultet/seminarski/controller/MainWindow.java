package rs.fakultet.seminarski.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import rs.fakultet.seminarski.model.Zaposleni;
import rs.fakultet.seminarski.utils.Stampac;
import service.ZaposleniServis;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;


public class MainWindow implements Initializable {

    //@formatter:off
    @FXML private TableView<Zaposleni> tableView;
    @FXML private TableColumn imeColumn;
    @FXML private TableColumn prezimeColumn;
    @FXML private TableColumn pozicijaColumn;
    @FXML private TableColumn brSatiColumn;
    @FXML private TableColumn vrSataColumn;
    @FXML private TableColumn zaradaColumn;
    @FXML private TextField filterTF;
    @FXML private Button filterButton;
    @FXML private Button dodajZaposlenogButton;
    @FXML private Button dodajRadneSateButton;
    @FXML private Button izbrisiZaposlenogButton;
    @FXML private Button izmeniPodatkeButton;
    @FXML private Button pretraziButton;
    @FXML private Button stampajButton;
    @FXML private ComboBox filterCB;
    @FXML private TextField imeTF;
    @FXML private TextField prezimeTF;
    @FXML private TextField pozicijaTF;
    @FXML private TextField brojRadnihSatiTF;
    @FXML private TextField vrednostSataTF;
    @FXML private TextField jmbgTF;
    private static boolean filterDugmeJeKliknuto = false;
    private static boolean pretragaDugmeJeKliknuto = false;
    //@formatter:on

    public void initialize(URL location, ResourceBundle resources) {
        ZaposleniServis zaposleniServis = new ZaposleniServis();
        podesiComboBoxVrednosti();
        podesiTabelu();
        popuniTabelu(zaposleniServis.dajZaposlene());

        dodajZaposlenogButton.setOnAction(e -> {
            if (!jmbgTF.getText().equals("") && !imeTF.getText().equals("") && !prezimeTF.getText().equals("") && !pozicijaTF.getText().equals("") && !brojRadnihSatiTF.getText().equals("") && !vrednostSataTF.getText().equals("")) {
                zaposleniServis.unesiZaposlenog(new Zaposleni(Integer.parseInt(jmbgTF.getText()), imeTF.getText(), prezimeTF.getText(), pozicijaTF.getText(), Integer.parseInt(brojRadnihSatiTF.getText()), Integer.parseInt((vrednostSataTF.getText()))));
                ocistiPolja();
                popuniTabelu(zaposleniServis.dajZaposlene());
            }
        });

        dodajRadneSateButton.setOnAction(e -> {
            if (!brojRadnihSatiTF.getText().equals("")) {
                Zaposleni zaposleni = tableView.getSelectionModel().getSelectedItem();
                zaposleni.setBrojRadnihSati(Integer.parseInt(brojRadnihSatiTF.getText()) + zaposleni.getBrojRadnihSati());
                zaposleniServis.izmeniZaposlenog(zaposleni);
                popuniTabelu(zaposleniServis.dajZaposlene());
                ocistiPolja();
            }
        });

        izbrisiZaposlenogButton.setOnAction(e -> {
            if (tableView.getSelectionModel().getSelectedItem() != null) {
                zaposleniServis.izbrisiZaposlenog(tableView.getSelectionModel().getSelectedItem().getJmbg());
                popuniTabelu(zaposleniServis.dajZaposlene());
            }
        });

        izmeniPodatkeButton.setOnAction(e -> {
            zaposleniServis.izmeniZaposlenog(dajIzmenjenogZaposlenog());
            popuniTabelu(zaposleniServis.dajZaposlene());
            ocistiPolja();
        });

        filterButton.setOnAction(e -> {
            if (filterCB.getSelectionModel().getSelectedItem() != null) {
                if (!filterDugmeJeKliknuto) {
                    filterDugmeJeKliknuto = true;
                    if (filterCB.getSelectionModel().getSelectedItem().equals("Plata veca od 500 evra")) {
                        popuniTabelu(zaposleniServis.filterPlataVecaOd500());
                    } else if (filterCB.getSelectionModel().getSelectedItem().equals("Plata manja od 500 evra")) {
                        popuniTabelu(zaposleniServis.filterPlataManjaOd500());
                    } else if (filterCB.getSelectionModel().getSelectedItem().equals("Broj radnih sati manji od 200")) {
                        popuniTabelu(zaposleniServis.filterPlataManjaOd500());
                    } else if (filterCB.getSelectionModel().getSelectedItem().equals("Broj radnih sati veci od 200")) {
                        popuniTabelu(zaposleniServis.filterBrRadnihSatiVecaOd200());
                    }
                } else {
                    filterDugmeJeKliknuto = false;
                    popuniTabelu(zaposleniServis.dajZaposlene());
                }
            }

        });

        pretraziButton.setOnAction(e -> {

            if (!pretragaDugmeJeKliknuto) {
                popuniTabelu(zaposleniServis.pretraziPoImenu(filterTF.getText()));
                pretragaDugmeJeKliknuto = true;

            } else {
                pretragaDugmeJeKliknuto = false;
                popuniTabelu(zaposleniServis.dajZaposlene());
            }

        });
        stampajButton.setOnAction(e->{
            new Stampac(zaposleniServis.dajZaposlene()).stampaj();
        });
    }

    private void podesiComboBoxVrednosti() {
        filterCB.getItems().clear();
        filterCB.getItems().addAll("Plata veca od 500 evra", "Plata manja od 500 evra", "Broj radnih sati manji od 200", "Broj radnih sati veci od 200");
    }

    private void podesiTabelu() {
        tableView.getSelectionModel().setCellSelectionEnabled(true);

        imeColumn.setCellValueFactory(new PropertyValueFactory<>("ime"));
        prezimeColumn.setCellValueFactory(new PropertyValueFactory<>("prezime"));
        pozicijaColumn.setCellValueFactory(new PropertyValueFactory<>("pozicija"));
        brSatiColumn.setCellValueFactory(new PropertyValueFactory<>("brojRadnihSati"));
        vrSataColumn.setCellValueFactory(new PropertyValueFactory<>("vrednostSata"));
        zaradaColumn.setCellValueFactory(new PropertyValueFactory<>("zarada"));
    }

    private void popuniTabelu(ArrayList<Zaposleni> zaposleni) {
        ObservableList<Zaposleni> listaZaposlenih = FXCollections.observableArrayList(zaposleni);
        tableView.setItems(listaZaposlenih);

    }

    private void ocistiPolja() {
        jmbgTF.clear();
        imeTF.clear();
        prezimeTF.clear();
        pozicijaTF.clear();
        brojRadnihSatiTF.clear();
        vrednostSataTF.clear();
    }

    public Zaposleni dajIzmenjenogZaposlenog() {

        Zaposleni zaposleni = tableView.getSelectionModel().getSelectedItem();
        if (!jmbgTF.getText().equals("")) {
            zaposleni.setJmbg(Integer.parseInt(jmbgTF.getText()));
        }
        if ((!imeTF.getText().equals(""))) {
            zaposleni.setIme(imeTF.getText());
        }
        if ((!prezimeTF.getText().equals(""))) {
            zaposleni.setPrezime(prezimeTF.getText());
        }
        if ((!pozicijaTF.getText().equals(""))) {
            zaposleni.setPozicija(pozicijaTF.getText());
        }
        if ((!brojRadnihSatiTF.getText().equals(""))) {
            zaposleni.setBrojRadnihSati(Integer.parseInt(brojRadnihSatiTF.getText()));
        }
        if ((!vrednostSataTF.getText().equals(""))) {
            zaposleni.setVrednostSata(Integer.parseInt((vrednostSataTF.getText())));
        }
        return zaposleni;
    }
}