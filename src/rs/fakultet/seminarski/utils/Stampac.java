package rs.fakultet.seminarski.utils;

import rs.fakultet.seminarski.model.Zaposleni;

import java.awt.*;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.util.ArrayList;

public class Stampac implements Printable {

    ArrayList<Zaposleni> lista;

    public Stampac(ArrayList<Zaposleni> z) {
        lista = z;
    }

    public int print(Graphics g, PageFormat pf, int page) {

        if (page > 0) {
            return NO_SUCH_PAGE;
        }

        Graphics2D grafika = (Graphics2D) g;
        grafika.translate(pf.getImageableX(), pf.getImageableY());
        g.drawString("jmbg", 10, 20);
        g.drawString("ime", 60, 20);
        g.drawString("prezime", 130, 20);
        g.drawString("pozicija", 200, 20);
        g.drawString("brojRadnihSati", 310, 20);
        g.drawString("vrednostSata", 410, 20);
        g.drawString("zarada", 500, 20);

        for (int i = 0, y = 50; i < lista.size(); i++, y = y + 20) {
            Zaposleni e = lista.get(i);
            g.drawString(Integer.toString(e.getJmbg()), 10, y);
            g.drawString(e.getIme(), 60, y);
            g.drawString(e.getPrezime(), 130, y);
            g.drawString(e.getPozicija(), 200, y);
            g.drawString(Integer.toString(e.getBrojRadnihSati()), 310, y);
            g.drawString(Integer.toString(e.getVrednostSata()), 410, y);
            g.drawString(Integer.toString(e.getZarada()), 500, y);
        }

        return PAGE_EXISTS;
    }

    public void stampaj() {
        PrinterJob posao = PrinterJob.getPrinterJob();
        posao.setPrintable(this);
        boolean ok = posao.printDialog();
        if (ok) {
            try {
                posao.print();
            } catch (PrinterException ex) {
                //u slucaju da se nije istampalo,
                //dobijamo exception
            }
        }
    }
}