package rs.fakultet.seminarski.repository;

import rs.fakultet.seminarski.model.Zaposleni;

import java.sql.*;
import java.util.ArrayList;

public class DBZaposleni {

    public void unesiNovogZaposlenog(Zaposleni zaposleni) {

        if (proveriDaliZaposleniPostojiUBazi(zaposleni.getJmbg())) {
            return;
        }

        String insertCovek = "INSERT INTO Covek(jmbg, ime, prezime) VALUES (?,?,?)";
        String insertZaposleni = "INSERT INTO Zaposleni(pozicija, brojRadnihSati, vrednostSata, zarada, jmbgZaposlenog) VALUES (?,?,?,?,?)";
        Connection conn = DBKonektor.konektujSe();
        PreparedStatement izjava;

        try {
            izjava = conn.prepareStatement(insertCovek);
            izjava.setInt(1, zaposleni.getJmbg());
            izjava.setString(2, zaposleni.getIme().toLowerCase());
            izjava.setString(3, zaposleni.getPrezime().toLowerCase());
            izjava.executeUpdate();
            izjava.close();

            izjava = conn.prepareStatement(insertZaposleni);
            izjava.setString(1, zaposleni.getPozicija());
            izjava.setInt(2, zaposleni.getBrojRadnihSati());
            izjava.setInt(3, zaposleni.getVrednostSata());
            izjava.setInt(4, zaposleni.getZarada());
            izjava.setInt(5, zaposleni.getJmbg());
            izjava.executeUpdate();
            izjava.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public boolean proveriDaliZaposleniPostojiUBazi(int jmbg) {
        String findEmail = "SELECT * FROM Zaposleni WHERE jmbgZaposlenog = '" + jmbg + "'";
        Statement izjava;
        ResultSet rs;
        String pozicija = "";
        try {
            Connection conn = DBKonektor.konektujSe();
            izjava = conn.createStatement();
            rs = izjava.executeQuery(findEmail);
            while (rs.next()) {
                pozicija = rs.getString("pozicija");
            }
            izjava.close();
            rs.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        if (pozicija.equals("")) {
            return false;
        } else {
            return true;
        }
    }

    public ArrayList<Zaposleni> dajSveZaposlene() {
        String nadjiCoveka = "SELECT * FROM Covek";
        String nadjiZaposlenog = "SELECT * FROM Zaposleni";
        ArrayList<Zaposleni> listaZaposlenih = new ArrayList<>();
        ResultSet rs;
        Statement statement;
        try {
            Connection conn = DBKonektor.konektujSe();
            statement = conn.createStatement();
            rs = statement.executeQuery(nadjiCoveka);
            while (rs.next()) {
                listaZaposlenih.add(new Zaposleni(rs.getInt("jmbg"), rs.getString("ime"), rs.getString("prezime")));
            }

            conn = DBKonektor.konektujSe();
            statement = conn.createStatement();
            rs = statement.executeQuery(nadjiZaposlenog);
            int i = 0;
            while (rs.next()) {
                Zaposleni zaposleni = listaZaposlenih.get(i);
                zaposleni.setZaposleni(zaposleni, rs.getString("pozicija"), rs.getInt("brojRadnihSati"), rs.getInt("vrednostSata"),
                        rs.getInt("zarada"));
                i++;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return listaZaposlenih;
    }

    public void izbrisiZaposlenog(int jmbg) {

        String izbrisiCoveka = "DELETE FROM Covek where jmbg ='" + jmbg + "'";
        String izbrisiZaposlenog = "DELETE FROM Zaposleni where jmbgZaposlenog ='" + jmbg + "'";
        PreparedStatement preparedStatement;
        try {

            preparedStatement = DBKonektor.konektujSe().prepareStatement(izbrisiCoveka);
            preparedStatement.executeUpdate();
            preparedStatement.close();
            preparedStatement = DBKonektor.konektujSe().prepareStatement(izbrisiZaposlenog);
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void izmeniZaposlenog(Zaposleni zaposleni) {

        String izmeniCoveka = "UPDATE Covek set jmbg = ?,ime = ?, prezime = ? WHERE jmbg = '" + zaposleni.getJmbg() + "'";
        String izmeniZaposlenog = "UPDATE Zaposleni set pozicija = ?,brojRadnihSati = ?, vrednostSata = ?, zarada = ? WHERE jmbgZaposlenog = '" + zaposleni.getJmbg() + "'";

        try {
            PreparedStatement preparedStatement = DBKonektor.konektujSe().prepareStatement(izmeniCoveka);
            preparedStatement.setInt(1, zaposleni.getJmbg());
            preparedStatement.setString(2, zaposleni.getIme());
            preparedStatement.setString(3, zaposleni.getPrezime());
            preparedStatement.executeUpdate();

            preparedStatement = DBKonektor.konektujSe().prepareStatement(izmeniZaposlenog);
            preparedStatement.setString(1, zaposleni.getPozicija());
            preparedStatement.setInt(2, zaposleni.getBrojRadnihSati());
            preparedStatement.setInt(3, zaposleni.getVrednostSata());
            preparedStatement.setInt(4, zaposleni.getZarada());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}