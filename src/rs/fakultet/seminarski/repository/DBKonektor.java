package rs.fakultet.seminarski.repository;

import rs.fakultet.seminarski.model.Zaposleni;
import service.ZaposleniServis;

import java.io.File;
import java.sql.*;

public class DBKonektor {

    private static Connection konekcija;
    private static DBKonektor dbKonektor;
    private static String lokacijaBaze = "jdbc:sqlite:evidencija.sqlite";

    private DBKonektor() {
    }

    public static DBKonektor dajObjekatBaze() {
        if (dbKonektor == null) {
            return dbKonektor = new DBKonektor();
        } else {
            return dbKonektor;
        }
    }

    public static Connection konektujSe() {

        if (konekcija == null) {
            try {

                konekcija = DriverManager.getConnection(lokacijaBaze);

            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        } else {
            return konekcija;
        }
        return konekcija;
    }


    public void proveriDaLiBazaPostoji() {
        String path = lokacijaBaze;
        File f = new File(path);
        if (!f.exists()) {
            napraviNovuBazu();
        }
    }


    public void napraviNovuBazu() {

        String url = lokacijaBaze;
        try {
            DriverManager.getConnection(url);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        napraviNoveTabele();
    }


    public void napraviNoveTabele() {

        String napraviTabeluCovek = "CREATE TABLE IF NOT EXISTS Covek(jmbg INTEGER PRIMARY KEY, ime varchar(255), prezime varchar(255));";

        String napraviTabeluZaposleni = "CREATE TABLE IF NOT EXISTS Zaposleni(pozicija VARCHAR(255),brojRadnihSati INTEGER,vrednostSata INTEGER,zarada INTEGER, jmbgZaposlenog INTEGER ,FOREIGN KEY(jmbgZaposlenog) REFERENCES Covek(jmbg));";

        Connection conn = konektujSe();
        try {
            Statement s = conn.createStatement();
            s.execute(napraviTabeluCovek);
            s = conn.createStatement();
            s.execute(napraviTabeluZaposleni);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        unesiPocetnePodatkeUBazu();
    }

    public void unesiPocetnePodatkeUBazu() {
        ZaposleniServis zaposleniServis = new ZaposleniServis();
        zaposleniServis.unesiZaposlenog(new Zaposleni(1, "Ivan", "Radojkovic", "Sef smene", 53, 12));
        zaposleniServis.unesiZaposlenog(new Zaposleni(2, "Jovan", "Avlic", "Radnik na traci", 23, 9));
        zaposleniServis.unesiZaposlenog(new Zaposleni(3, "Aca", "Ivkovic", "Radnik na traci", 42, 9));
        zaposleniServis.unesiZaposlenog(new Zaposleni(4, "Nemanja", "Nikolic", "Kontrolor", 43, 10));
    }
}