package rs.fakultet.seminarski.model;

public class Zaposleni {

    private int jmbg;
    private String ime;
    private String prezime;
    private String pozicija;
    private int brojRadnihSati;
    private int vrednostSata;
    private int zarada;

    public Zaposleni(int jmbg, String ime, String prezime, String pozicija, int brojRadnihSati, int vrednostSata, int zarada) {
        this.jmbg = jmbg;
        this.ime = ime;
        this.prezime = prezime;
        this.pozicija = pozicija;
        this.brojRadnihSati = brojRadnihSati;
        this.vrednostSata = vrednostSata;
        this.zarada = vrednostSata * brojRadnihSati;
    }

    public Zaposleni(int jmbg, String ime, String prezime, String pozicija, int brojRadnihSati, int vrednostSata) {
        this.jmbg = jmbg;
        this.ime = ime;
        this.prezime = prezime;
        this.pozicija = pozicija;
        this.brojRadnihSati = brojRadnihSati;
        this.vrednostSata = vrednostSata;
        this.zarada = brojRadnihSati * vrednostSata;
    }

    public Zaposleni(int jmbg, String ime, String prezime) {
        this.jmbg = jmbg;
        this.ime = ime;
        this.prezime = prezime;
    }

    public void setZaposleni(Zaposleni zaposleni, String pozicija, int brojRadnihSati, int vrednostSata, int zarada) {

        zaposleni.pozicija = pozicija;
        zaposleni.brojRadnihSati = brojRadnihSati;
        zaposleni.vrednostSata = vrednostSata;
        zaposleni.zarada = vrednostSata * brojRadnihSati;
    }

    public Zaposleni() {
    }

    public int getJmbg() {
        return jmbg;
    }

    public void setJmbg(int jmbg) {
        this.jmbg = jmbg;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getPozicija() {
        return pozicija;
    }

    public void setPozicija(String pozicija) {
        this.pozicija = pozicija;
    }

    public int getBrojRadnihSati() {
        return brojRadnihSati;
    }

    public void setBrojRadnihSati(int brojRadnihSati) {
        this.brojRadnihSati = brojRadnihSati;
    }

    public int getVrednostSata() {
        return vrednostSata;
    }

    public void setVrednostSata(int vrednostSata) {
        this.vrednostSata = vrednostSata;
    }

    public int getZarada() {
        return zarada;
    }

    public void setZarada(int zarada) {
        this.zarada = vrednostSata * brojRadnihSati;
    }
}
